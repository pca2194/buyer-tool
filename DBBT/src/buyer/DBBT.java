
//test

package buyer;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;
//*Author(s) --- Java Beans ---
//*CSE1325-001
//*Resources 
//**Sample Code(s) via Professor Becker


public class DBBT {

    Scanner input;
    Formatter output;
    char displayArray[][];
    char boneArray[][];
    char buyerArray[][];
    char continentArray[][];
    ArrayList <MapNode> mapArrayList;
    ArrayList <BoneNode> boneArrayList;
    ArrayList <Buyer> buyerArrayList;
    ArrayList <Continent> continentArrayList;
    
    int loaded=0;
    int loaded2=0;
    int seller=0;
    int cont=0;
    
    public DBBT() {
        
        input=new Scanner(System.in);
        output=new Formatter(System.out);
        mapArrayList=new ArrayList<>(1200);
        displayArray=new char[60][20];//Columnn by Row
        
        boneArrayList=new ArrayList<>();
        boneArray=new char[6][20];
        
        buyerArrayList=new ArrayList<>();
        buyerArray=new char[3][1];
        
        continentArrayList=new ArrayList<>();
        continentArray=new char[3][1000];
        
} //*setting some global variables under a class
    
    public void LoadMap() //*loads the map off a text file into an array
    {
        String filename="Map.txt";
        File mapFile=new File(filename);
        String line=null;
        String dataset[];
        int row;
        int col;
        int land;
        MapNode tempNode;
        
        try
        {
        Scanner mapReader=new Scanner(mapFile);
        
        
        while (mapReader.hasNextLine())
        {
            line=mapReader.nextLine();
            dataset=line.split(",");
            col=Integer.parseInt(dataset[0]);
            row=Integer.parseInt(dataset[1]);
            land=Integer.parseInt(dataset[2]);
            tempNode=new MapNode(col,row,land);
            mapArrayList.add(tempNode);
        }
        mapReader.close();
        }
        catch (FileNotFoundException fnfE)
        {
            fnfE.printStackTrace();
        }  
    }
    
    public void showMap() //*converts the array into a 2D array, then prints it
    {
        int mapLoop;
        int mapMax;
        MapNode tempNode;
        
        mapMax=mapArrayList.size();
        
        //Translate Array List to 2D Array
        for (mapLoop=0;mapLoop<mapMax;mapLoop++)
        {
            tempNode=mapArrayList.get(mapLoop);
            //screen.format("%d,%d,%d",tempNode.col,tempNode.row,tempNode.land);
        
            switch(tempNode.land)
            {
                case 0:
                    displayArray[tempNode.col][tempNode.row]='.';
                    break;
                case 1:
                    displayArray[tempNode.col][tempNode.row]='*';
                    break;
                     }
        }
        
        //*prints the 2D array
        int rowLoop;
        int colLoop;
        char symbol;
        
        output.format("                  JavaBeans Bone WholeSale                  \n");
        
        for (rowLoop=0;rowLoop<20;rowLoop++)
        {
            for(colLoop=0;colLoop<60;colLoop++)
            {
                symbol=displayArray[colLoop][rowLoop];
                output.format("%c",symbol);
                
            }
            output.format("\n");
        }
        
        
        
    }    
    
    public void showMap2() //*a second showMap which is triggered if the Bone file has been loaded
    {
        //*Did a second showMap with a load trigger so that I didn't have to worry about 
        //*showMap failing if bones aren't loaded, but have bone values in the code.
        int mapLoop;
        int mapMax;
        int listMax;
        int key = 0;
        int counter;
        MapNode tempNode;
        BoneNode tempNode2;
        Buyer tempNode3;
        Buyer tempNode4;
        
        double dist_1;
        int dist_2;
        double temp, temp2;
        
        listMax=boneArrayList.size();
        
        mapMax=mapArrayList.size();
        
        //Translate Array List to 2D Array
        for (mapLoop=0;mapLoop<mapMax;mapLoop++)
        {
            tempNode=mapArrayList.get(mapLoop);
            //screen.format("%d,%d,%d",tempNode.col,tempNode.row,tempNode.land);
        
            switch(tempNode.land)
            {
                case 0:
                    displayArray[tempNode.col][tempNode.row]='.';
                    break;
                case 1:
                    displayArray[tempNode.col][tempNode.row]='*';
                    break;
                     }
        }
        //Show the 2D array
        int rowLoop;
        int colLoop;
        char symbol;
        
        output.format("                  JavaBeans Bone WholeSale                  \n");
        //*Check to see if bone coordinates matches with map coordinates, and if so, replaces with 'X' or '$' depending on sold value
        for (rowLoop=0;rowLoop<20;rowLoop++)
        {
            for(colLoop=0;colLoop<60;colLoop++)
            {
                if(loaded2 == 1)
                {
                    tempNode3=buyerArrayList.get(0);
                        if(rowLoop == tempNode3.rowIndex)
                        {
                            if(colLoop == tempNode3.colIndex)
                            {
                                key = 3;
                            }
                        }
                }
                if(seller == 2)
                {
                    tempNode3=buyerArrayList.get(1);
                        if(rowLoop == tempNode3.rowIndex)
                        {
                            if(colLoop == tempNode3.colIndex)
                            {
                                key = 4;
                            }
                        }
                }
                for(counter=0;counter<listMax;counter++)
                {
                    tempNode2=boneArrayList.get(counter);                          
                    if(rowLoop == tempNode2.lad)
                    {
                        if(colLoop == tempNode2.lon)
                        {
                            if(tempNode2.sold == 0)
                            {
                                key = 1;
                            }
                            else
                            {
                                key = 2;
                            }
                        }
                    }
                }
                if(key > 0)
                {
                    if(key == 1)
                        output.format("X");
                    else if(key == 2)
                        output.format("$");
                    else if(key == 3)
                        output.format("B");
                    else
                        output.format("S");
                }
                else
                {
                    symbol=displayArray[colLoop][rowLoop];
                    output.format("%c",symbol);
                }
                counter = 0;
                key = 0;
                
            }
            output.format("\n");
        }               
        if(seller==2)
        {
            tempNode3 = buyerArrayList.get(0);
            tempNode4 = buyerArrayList.get(1);
            
            temp = tempNode3.lon - tempNode4.lon;
            temp2 = tempNode3.lad - tempNode4.lad;
            temp = Math.pow(temp, 2);
            temp2 = Math.pow(temp2, 2);
            dist_1 = Math.sqrt(temp + temp2);
            output.format("Distance between Buyer and Seller is %.1f\n", dist_1);
        }
    } 
    
    public void Buy() //*initiates menu to buy a bone
    {
        BoneNode tempNode;
        int listMax;
        int length;
        
        listMax=boneArrayList.size();
        //*prints out list of bones for user
        for(length=0;length<listMax;length++)
        {
            tempNode = boneArrayList.get(length);
            
            output.format("%s,", tempNode.name);
            output.format("%.2f,", tempNode.price);
            output.format("%d\n", tempNode.ID);
            
            
        }

        BoneBuy();
    }
    
    public void BoneBuy() //*method that actually purchases the bone, changing its sold value
    {
        BoneNode tempNode;
        int listMax;
        int length;
        int IDin;
        
        listMax=boneArrayList.size();
        
        output.format("Input the ID of the Desired Bone.\n");
        IDin = input.nextInt();
        input.nextLine();
        //*checks for inputted ID #, if matching, will change value of corresponding bone to sold
        for(length=0;length<listMax;length++)
        {
            tempNode = boneArrayList.get(length);
            
            if(IDin == tempNode.ID)
            {
                tempNode.sold = 1;
            }
        }
        output.format("Thank you for purchasing!\n");
    }
    
    public void Save() //*saves the current arraylist of bone data into a new textfile
    {
        int listMax;
        BoneNode tempNode;
        int length;
        int width;
        
        String filename;
        
        output.format("Enter the Bone List FileName: \n");
        filename = input.next();
        input.nextLine();
        listMax=boneArrayList.size();
        
        File outputfile=new File(filename);
        
        
        try {
            Formatter fileWriter=new Formatter(outputfile);
            
            for (length=0;length<listMax;length++)
            {
                tempNode=boneArrayList.get(length);
                fileWriter.format("%s,", tempNode.name);
                fileWriter.format("%.2f,", tempNode.price);
                fileWriter.format("%d,", tempNode.sold);
                fileWriter.format("%d,", tempNode.lon);
                fileWriter.format("%d,", tempNode.lad);
                fileWriter.format("%d\r\n", tempNode.ID); 
            }
            fileWriter.close();
            
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DBBT.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void SaveBuyer(File outputFile) //*saves the current Buyer into a new textfile
    {
        Buyer tempNode;
        
        try {
            try (Formatter fileWriter=new Formatter(outputFile)) {
            tempNode=buyerArrayList.get(0);
            fileWriter.format("%f,", tempNode.lon);
            fileWriter.format("%f,", tempNode.lad);
            fileWriter.format("%d,", tempNode.rowIndex);
            fileWriter.format("%d,", tempNode.colIndex);
            fileWriter.format("%s\r\n", tempNode.name2); 
        }
            
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DBBT.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void Load() //*loads a textfile into an arraylist
    {
        String filename;
        output.format("Enter the FileName: \n");
        filename= input.next();
        input.nextLine();
        File mapFile=new File(filename);
        String line=null;
        String dataset[];
        String name;
        float price;
        int sold;
        int lon;
        int lad;
        int ID;
        
        boneArrayList.clear();
        
        BoneNode tempNode;
        
        try
        {
        Scanner listReader=new Scanner(mapFile);
        
        
        while (listReader.hasNextLine())
        {
            line=listReader.nextLine();
            //Data here screen.format(line);
            dataset=line.split(",");
            name=String.valueOf(dataset[0]);
            price=Float.parseFloat(dataset[1]);
            sold=Integer.parseInt(dataset[2]);
            lon=Integer.parseInt(dataset[3]);
            lad=Integer.parseInt(dataset[4]);
            ID=Integer.parseInt(dataset[5]);
            tempNode=new BoneNode(name,price,sold,lon,lad,ID);
            boneArrayList.add(tempNode);
        }
        listReader.close();
        }
        catch (FileNotFoundException fnfE)
        {
            fnfE.printStackTrace();
        }
        loaded = 2;
    }
    
    public void LoadBuyer() //*loads a Buyer into its arraylist
    {
        String filename;
        output.format("Enter the Buyer FileName: \n");
        filename= input.next();
        input.nextLine();
        File mapFile=new File(filename);
        String line=null;
        String dataset[];
        String name2;
        int colIndex;
        int rowIndex;
        double lon;
        double lad;

        boneArrayList.clear();
        
        Buyer tempNode;
        
        try
        {
        Scanner listReader=new Scanner(mapFile);
        
        
        while (listReader.hasNextLine())
        {
            line=listReader.nextLine();
            //Data here screen.format(line);
            dataset=line.split(",");
            lon=Double.parseDouble(dataset[0]);
            lad=Double.parseDouble(dataset[1]);
            rowIndex=Integer.parseInt(dataset[2]);
            colIndex=Integer.parseInt(dataset[3]);
            name2=String.valueOf(dataset[4]);
            tempNode=new Buyer(lon,lad,rowIndex,colIndex,name2);
            buyerArrayList.add(tempNode);
        }
        listReader.close();
        }
        catch (FileNotFoundException fnfE)
        {
            fnfE.printStackTrace();
        }
        loaded2 = 1;
        seller++;
    }
    
    public void LoadContinents() //*loads a textfile into an arraylist
    {
        String filename;
        filename= "continents.txt";
        File mapFile=new File(filename);
        String line=null;
        String dataset[];
        int colIndex;
        int rowIndex;
        int land;

        
        Continent tempNode;
        
        try
        {
        Scanner listReader=new Scanner(mapFile);
        
        
        while (listReader.hasNextLine())
        {
            line=listReader.nextLine();
            //Data here screen.format(line);
            dataset=line.split(",");
            colIndex=Integer.parseInt(dataset[0]);
            rowIndex=Integer.parseInt(dataset[1]);
            land=Integer.parseInt(dataset[2]);
            tempNode=new Continent(colIndex,rowIndex,land);
            continentArrayList.add(tempNode);
        }
        listReader.close();
        }
        catch (FileNotFoundException fnfE)
        {
            fnfE.printStackTrace();
        }
    }
    
    public void BuyerCreate(double new_lon, double new_lad, String new_name2) {
        Buyer tempNode;
        int new_colIndex = (int)new_lon + 180;
        int new_rowIndex = (int)new_lad*-1;
        new_rowIndex = new_rowIndex+90;
        new_colIndex = new_colIndex/6;
        new_rowIndex = new_rowIndex/9;
        if(new_colIndex == 60)
            new_colIndex--;
        if(new_rowIndex == 20)
            new_rowIndex--;
        
        tempNode=new Buyer(new_lon,new_lad,new_rowIndex,new_colIndex,new_name2);
        buyerArrayList.add(tempNode);
        loaded2=1;
    } //*Creates a Buyer
    
    public void BuyerUpdate() {
        Buyer tempNode;

        tempNode = buyerArrayList.get(0);

        output.format("Enter new longitude.\n");
        tempNode.colIndex = (input.nextInt()+180)/6;
        output.format("Enter new latitude.\n");
        tempNode.rowIndex = ((input.nextInt()*-1)+90)/9;

        input.nextLine();
    } //*Updates a Buyer
    
    public void BuyerMenu() {
        
        String key2;
        boolean done;
        
        done=false;
        
        while(!done)
        {
        output.format("What do you want to do?\n");
        output.format("1. Create Buyer.\n");
        output.format("2. Update Location.\n");
        output.format("3. Quit.\n");
        output.format(">");
        
        key2=input.nextLine();
        
        switch(key2)
        {
            case "1":
                BuyerCreate();
                done=true;
                break;   
            case "2":
                BuyerUpdate();
                done=true;
                break;   
            case "3":
                done=true;
                break;
                
                
            default:
                output.format("Please Select One.\n");
                break;
        
        
        }       
        
        }
} //*Buyer Menu to create/update a Buyer
    
    public void Scramble() //*Randomization of bones within continents
    {
        int listMax,listMax2;
        BoneNode tempNode;
        Continent tempNode2;
        int length, length2;
        int cswitch=0;
        
        Random rand = new Random();
        
        listMax=boneArrayList.size();
        listMax2=continentArrayList.size();
        for (length=0;length<listMax;length++)
            {
                while(cswitch != 1)
                {
                    tempNode=boneArrayList.get(length);
                    tempNode.lad = rand.nextInt(19)+1;
                    tempNode.lon = rand.nextInt(59)+1;
                    for(length2=0;length2<listMax2;length2++)
                    {
                        tempNode2=continentArrayList.get(length2);
                        if(tempNode.lon == tempNode2.colIndex)
                        {
                            if(tempNode.lad == tempNode2.rowIndex)
                                cswitch = 1;
                        }
                    }
                }
                cswitch = 0;
                
            }
        
        
    }
    
    public void menu() //*opening menu that asks what you want to do
    {
        String key;
        boolean done;
        
        done=false;
        
        while(!done)
        {
        output.format("Welcome to JavaBeans Bone Wholesale!\n");
        output.format("What do you want to do?\n");
        output.format("1. Load the world map.\n");
        output.format("2. Load continents.\n");
        output.format("3. Show the World Map with Dinosaur Bones.\n");
        output.format("4. Buy a Dinosaur Bone.\n");
        output.format("5. Save Files.\n");
        output.format("6. Load Bones.\n");
        output.format("7. Load Buyer.\n");
        output.format("8. Buyer Menu.\n");
        output.format("9. Scramble.\n");
        output.format("10. Quit.\n");
        output.format(">");
        
        key=input.nextLine();
        
        switch(key)
        {
            case "1":
                LoadMap();
                break;   
            case "2":
                LoadContinents();
                break;
            case "3":
                if(loaded == 0 && loaded2 == 0)
                    showMap();
                else
                    showMap2();
                break;
            case "4":
                if(loaded == 0)
                    output.format("No bones exist.\n");
                else
                    Buy();
                break;   
            case "5":
                if(loaded == 1)
                {
                    Save();
                if(loaded2 == 1)
                    SaveBuyer();
                }
                else if(loaded2 == 1)
                    SaveBuyer();
                else
                    output.format("No existing data to save.\n");
                break;    
            case "6":
                Load();
                break;
            case "7":
                LoadBuyer();
                break;
            case "8":
                BuyerMenu();
                break;
            case "9":
                Scramble();
                break;
            case "10":
                done=true;
                break;
                
                
            default:
                output.format("Please Select One.\n");
                break;
        
        
        }       
        
        }
        
    }
    
    public class MapNode {
    
    int row;
    int col;
    int land;
    
    public MapNode(int col, int row, int land)
    {
        this.row=row;
        this.col=col;
        this.land=land;
    }
    
} //*node that records data from the map
    
    public class BoneNode {
        
    String name;
    float price;
    int sold;
    int lon;
    int lad;
    int ID;
    
    public BoneNode(String name, float price, int sold, int lon, int lad, int ID)
    {
        this.name=name;
        this.price=price;
        this.sold=sold;
        this.lon=lon;
        this.lad=lad;
        this.ID=ID;
    }
} //*node that records data from the bone textfile
    
    public class Buyer {
    
    double lon;
    double lad;
    int rowIndex;
    int colIndex;
    String name2;
    
    public Buyer(double lon, double lad, int rowIndex, int colIndex, String name2)
    {
        this.lon = lon;
        this.lad = lad;
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
        this.name2 = name2;
    }
} //*Buyer class/constructor
    
    public class Continent {
        
    int colIndex;
    int rowIndex;
    int land;
    
    public Continent(int colIndex, int rowIndex, int land)
    {
        this.colIndex = colIndex;
        this.rowIndex = rowIndex;
        this.land = land;
    }
} //*Continent Constructor

    
    public static void main(String[] args) {
        // TODO code application logic here
        DBBT dbbt=new DBBT();
        MainGUI main = new MainGUI(dbbt);
        main.setSize(500,500);
        main.setVisible(true);
    }
    
}
    
